package dojo.tennis;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TennisKataTest {

    public static final String PLAYER_1 = "player 1";
    public static final String PLAYER_2 = "player 2";

    @org.junit.Test
    public void loveAll() throws Exception {
        TennisKata tennisKata = new TennisKata();

        assertEquals(TennisKata.LOVE_ALL, tennisKata.getScore());
    }

    @Test
    public void playerScored() throws Exception {
        TennisKata tennisKata = new TennisKata();

        tennisKata.wonPoint(PLAYER_1);
        assertEquals(TennisKata.FIFTEEN_LOVE, tennisKata.getScore());
    }

    @Test
    public void player2Scored() throws Exception {
        TennisKata tennisKata = new TennisKata();

        tennisKata.wonPoint(PLAYER_2);
        assertEquals("love-fifteen", tennisKata.getScore());
    }

    @Test
    public void fifteenAll() throws Exception {
        TennisKata tennisKata = new TennisKata();
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_2);

        assertEquals("fifteen-all", tennisKata.getScore());
    }

    @Test
    public void deuce() throws Exception {
        TennisKata tennisKata = new TennisKata();

        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);

        assertEquals("deuce", tennisKata.getScore());

    }

    @Test
    public void advantagePlayer1() throws Exception {
        TennisKata tennisKata = new TennisKata();

        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_1);

        assertEquals("advantage player 1", tennisKata.getScore());
    }

    @Test
    public void advantagePlayer2() throws Exception {
        TennisKata tennisKata = new TennisKata();

        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_1);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);
        tennisKata.wonPoint(PLAYER_2);

        assertEquals("advantage player 2", tennisKata.getScore());
    }
}