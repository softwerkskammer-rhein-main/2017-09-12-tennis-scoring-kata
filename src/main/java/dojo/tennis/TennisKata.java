package dojo.tennis;

public class TennisKata {
    public static final String LOVE_ALL = "love-all";
    public static final String FIFTEEN_LOVE = "fifteen-love";
    String[] scores = new String[]{"love", "fifteen", "thirty", "forty"};
    int scorePlayer1 = 0;
    int scorePlayer2 = 0;

    public String getScore() {
        if (scorePlayer1 < 3) {
            return scoreUpToForty();
        } else {
            return scoreDeuce();
        }
    }

    private String scoreDeuce() {
        if (scorePlayer1 == scorePlayer2) {
            return "deuce";
        } else if (scorePlayer1 > scorePlayer2) {
            return "advantage player 1";
        } else {
            return "advantage player 2";
        }
    }

    private String scoreUpToForty() {
        String score;
        score = scores[scorePlayer1];
        if (scorePlayer1 == scorePlayer2) {
            score += "-all";
        } else {
            score += "-" + scores[scorePlayer2];
        }
        return score;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals("player 1")) {
            scorePlayer1++;
        } else {
            scorePlayer2++;
        }
    }
}
