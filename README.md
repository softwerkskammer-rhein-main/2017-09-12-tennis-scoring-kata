# Tennis Scoring Kata #

Implement a simple tennis game

Rules:

- Scores from zero to three points are described as “love”, “fifteen”, “thirty”, and “forty” respectively.
- If both players have the same score, then the score is “love-all”, “fifteen-all” and “thirty-all”.
- If at least three points have been scored by each side and a player has one more point than his opponent, the score of the game is “advantage” for the player in the lead.
- If at least three points have been scored by each player, and the scores are equal, the score is “deuce”.
- A game is won by the first player to have won at least four points in total and at least two points more than the opponent.

Source:

- https://de.wikipedia.org/wiki/Tennis#Z.C3.A4hlweise
- https://technologyconversations.com/2014/04/23/java-tutorial-through-katas-tennis-game-easy/